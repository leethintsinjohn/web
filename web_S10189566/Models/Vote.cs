﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Web_S10188704.Models
{
    public class Vote
    {
        public int BookId { get; set; }

        public string Justification { get; set; }

    }
}
